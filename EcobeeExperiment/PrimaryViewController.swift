//
//  PrimaryViewController.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 1/27/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import UIKit

class PrimaryViewController: UIViewController {
    
    @IBOutlet weak var hoursRunLabel: UILabel!
    @IBOutlet weak var calcCostLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
