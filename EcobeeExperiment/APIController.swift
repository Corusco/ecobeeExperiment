//
//  APIController.swift
//  EcobeeExperiment
//
//  Created by Justin Huntington on 1/27/16.
//  Copyright © 2016 CVLCD. All rights reserved.
//

import Foundation

let kClientID = NSURLQueryItem (name: "client_id", value: "kFjrKrJhLFk9c5h3H8rBCIL1pT3Kh11y")
let baseBetaURLString = "beta.ecobee.com"
let authorizeURLString = "api.ecobee.com"
let apiVersionInt = 1
let kResponseQuery = NSURLQueryItem (name: "response_type", value: "ecobeePin")
let kScope = NSURLQueryItem (name: "scope", value: "smartRead")
var authCode = String()

func getPIN (){
    let components = NSURLComponents()
    components.scheme = "https"
    components.host = authorizeURLString
    components.queryItems = [kResponseQuery, kClientID, kScope]
    components.path = "/authorize"
    print(components.URL)
    
    if let url = components.URL {
        
        let dataTask = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
    
            if let error = error {
                print ("Error: \(error.localizedDescription): \(error.userInfo)")
            }
            
            guard let data = data else {
                return
            }
            
            var jsonObject : AnyObject
            
            if let jsonObject = data {
                
            }
            
        })
    }
}
